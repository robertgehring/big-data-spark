package com.refactorlabs.cs378.assign11;

/* -------------------------------------------------------*/
/* Robert Gehring (UTEid: rjg2358) (CS Username: rgehring)*/
/* Assignment 3 - cs 378 Big Data Programming Spring 2015 */
/* -------------------------------------------------------*/

import java.io.Serializable;

public class VerseId implements Comparable<VerseId>, Serializable
{
    private String book;
    private int chapter;
    private int verse;

    public VerseId(String book, int chapter, int verse)
    {
        this.book=book;
        this.chapter=chapter;
        this.verse=verse;
    }

    public VerseId(String verseString)
    {
        String[] verseStringParsed = verseString.split(":",3);
        this.book = verseStringParsed[0];
        this.chapter = Integer.parseInt(verseStringParsed[1]);
        this.verse = Integer.parseInt(verseStringParsed[2]);
    }

    /* See java's comparable interface for definition */
    public int compareTo(VerseId v)
    {
        int booksCompared = this.book.toLowerCase().compareTo(v.book.toLowerCase()); //neg if this<v
        /* If the books differ make a decide from there */
        if (booksCompared == 0)
        {
            if(this.chapter == v.chapter)
            {
                /* Book and chapter are the same -> decide on verse*/
                return this.verse - v.verse;
            }
            else
            {
                /* Books the same but the chapters differ -> decide on chapter*/
                return this.chapter-v.chapter;
            }
        }
        else
        {
            /* Books differ -> decide on book*/
            return booksCompared;
        }
    }

    /*String representation of a verseid */
    @Override
    public String toString()
    {
        return new String(this.book + ":" + this.chapter + ":" + this.verse);
    }

}
