package com.refactorlabs.cs378.assign11;

import com.refactorlabs.cs378.utils.Utils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import scala.Tuple2;

import java.util.*;

/* Robert Gehring */

public class InvertedIndex
{
    public static void main(String[] args)
    {
        Utils.printClassPath();

        String inputFilename = args[0];
        String outputFilename = args[1];

        // Create a Java Spark context
        SparkConf conf = new SparkConf().setAppName(InvertedIndex.class.getName()).setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Load the input data
        JavaRDD<String> input = sc.textFile(inputFilename);

        /* This function will be used to take a verse and map it like map reduce to a <word, Iterable<Verse> */
        PairFlatMapFunction<String,String,Set<VerseId>> verseBinFunction = new PairFlatMapFunction<String, String, Set<VerseId>>()
        {
            @Override
            public Iterable<Tuple2<String, Set<VerseId>>> call(String s) throws Exception {
                try {
                    String[] verseWords = s.toLowerCase().split(" ");


                    Set<String> wordsInVerse = new HashSet<String>();
                    /* Get all the distinct words in this verse */
                    for (int i = 1; i < verseWords.length; i++)
                    {
                        if (!Utils.standardize(verseWords[i]).isEmpty())
                            wordsInVerse.add(Utils.standardize(verseWords[i]));
                    }


                    /* VerseId in Set to be used for all the words */
                    Set<VerseId> verseSet = new HashSet<VerseId>();
                    verseSet.add(new VerseId(verseWords[0])); /* We've just seen one verse for this word so far */

                    List<Tuple2<String, Set<VerseId>>> wordVerseTuples = new ArrayList<Tuple2<String, Set<VerseId>>>();
                    /* For each word show that it was seen by in this verse and add it to the return iterable */
                    for (String word : wordsInVerse)
                    {
                        wordVerseTuples.add(new Tuple2<String, Set<VerseId>>(word, verseSet));
                    }

                    return wordVerseTuples;
                }
                /* Catch blank lines */
                catch (ArrayIndexOutOfBoundsException e) {return new ArrayList<Tuple2<String, Set<VerseId>>>();}
            }
        };

        /* To be used to reduce the pair rdd that represents all the words and the their set(s) of verses */
        Function2<Set<VerseId>, Set<VerseId>, Set<VerseId>> sumFunction = new Function2<Set<VerseId>, Set<VerseId>, Set<VerseId>>() {
            @Override
            public Set<VerseId> call(Set<VerseId> verseSet1, Set<VerseId> verseSet2) throws Exception {
                Set<VerseId> unionSet = new TreeSet<VerseId>();
                unionSet.addAll(verseSet1);
                unionSet.addAll(verseSet2);
                return unionSet;
            }
        };

        /* Will produce all the words as they keys and a set of all the verses they occurred in. Sorted per specs of assignment.*/
        JavaPairRDD<String, Set<VerseId>> wordsWithVerse = input.flatMapToPair(verseBinFunction).reduceByKey(sumFunction).sortByKey();

        wordsWithVerse.saveAsTextFile(outputFilename);

        /* Function to map the key value pairs to straight tuples so we can work with a java rdd */
        Function<Tuple2<String, Set<VerseId>>, Tuple2<String, Set<VerseId>>> mapTuples = new Function<Tuple2<String, Set<VerseId>>, Tuple2<String, Set<VerseId>>>() {
            @Override
            public Tuple2<String, Set<VerseId>> call(Tuple2<String, Set<VerseId>> stringSetTuple2) throws Exception {
                return stringSetTuple2;
            }
        };

        /* InvertedIndex pair rdd in java rdd form */
        JavaRDD<Tuple2<String, Set<VerseId>>> invertedIndexRdd = wordsWithVerse.map(mapTuples);

        /* To be used to sort the tuples by the size of their set (The number of verses) */
        Function<Tuple2<String, Set<VerseId>>, Integer> mapSortFunction = new Function<Tuple2<String, Set<VerseId>>, Integer>(){
            @Override
            public Integer call(Tuple2<String, Set<VerseId>> tuple) throws Exception {
                return tuple._2.size();
            }
        };

        /* Sorted by number of verses and if they have the same number of verses by key (already done for us in java pair rdd above */
        /* When it was sorted by key                                                                                                */
        JavaRDD<Tuple2<String, Set<VerseId>>> invertedIndexNumVersesSorted = invertedIndexRdd.sortBy(mapSortFunction, false, 1);

        invertedIndexNumVersesSorted.saveAsTextFile(outputFilename + "/ExtraCredit");

        sc.stop();



    }
}
