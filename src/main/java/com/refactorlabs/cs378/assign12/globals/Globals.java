package com.refactorlabs.cs378.assign12.globals;

/* Globals for assignment 12 */
public class Globals
{
    public static int userIdIndex = 0;
    public static int eventTypeIndex = 1;
    public static int referringDomainIndex = 4;
    public static int timeStampIndex = 5;
}
