package com.refactorlabs.cs378.assign12.domain;

import java.io.Serializable;
import java.util.Date;

/* Event class for assignment 12 */
public class Event implements Serializable, Comparable<Event> {
    private String eventType;
    private String eventTimestamp;

    public Event(String eventType, String timeStamp) { this.eventType = eventType; this.eventTimestamp = timeStamp; }

    public void setEventType(String eventType) { this.eventType = eventType; }

    public void setEventTimeStamp(String eventTimeStamp) { this.eventTimestamp = eventTimeStamp; }

    public String getEventType() { return this.eventType; }

    public String getEventTimestamp() { return this.eventTimestamp; }

    public int compareTo(Event e)
    {
        return this.getEventTimestamp().compareTo(e.getEventTimestamp());
    }

    public String toString() { return "<" + eventType + "," + eventTimestamp + ">";}


}


