package com.refactorlabs.cs378.assign12.resources;

import java.io.Serializable;
import java.util.Comparator;

import scala.Tuple2;

/* Robert Gehring, Univesity of Texas, Austin        */
/* Big Data Programming - Spring 2015 - David Franke */
/* UTEID: rjg2358                                    */

/* To be used to compare user_ids for assignment12 */
public class UserKeyComparator implements Comparator<Tuple2<String, String>>, Serializable
{

    @Override
    public int compare(Tuple2<String, String> thisT, Tuple2<String, String> thatT)
    {
        return Integer.parseInt(thisT._1())  - Integer.parseInt(thatT._1());
    }
}
