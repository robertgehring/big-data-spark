package com.refactorlabs.cs378.assign12.resources;

import org.apache.spark.Partitioner;
import scala.Tuple2;

/* Robert Gehring, Univesity of Texas, Austin        */
/* Big Data Programming - Spring 2015 - David Franke */
/* UTEID: rjg2358                                    */

/* Custom paritioner based off the referring domain value in the session */
public class CustomPartitioner extends Partitioner {

    public int numPartitions()
    {
        return 10;
    }

    public int getPartition(Object keyObj) {
        Tuple2<String, String> key = (Tuple2<String, String>) keyObj;
        return Math.abs(key._2().hashCode() % 10);
    }

}
