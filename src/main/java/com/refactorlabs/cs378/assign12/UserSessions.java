package com.refactorlabs.cs378.assign12;

import com.refactorlabs.cs378.assign12.domain.Event;
import com.refactorlabs.cs378.assign12.globals.Globals;
import com.refactorlabs.cs378.assign12.resources.CustomPartitioner;
import com.refactorlabs.cs378.assign12.resources.UserKeyComparator;
import com.refactorlabs.cs378.utils.Utils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.*;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.TreeSet;


/* Robert Gehring, Univesity of Texas, Austin        */
/* Big Data Programming - Spring 2015 - David Franke */
/* UTEID: rjg2358                                    */
public class UserSessions
{
    public static void main(String[] args)
    {
        Utils.printClassPath();

        String inputFilename = args[0];
        String outputFilename = args[1];

        // Create a Java Spark context
        SparkConf conf = new SparkConf().setAppName(UserSessions.class.getName()).setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Load the input data
        JavaRDD<String> input = sc.textFile(inputFilename);

        /* Transformation 1 - line to session tuple */
        PairFunction<String, Tuple2<String,String>, ArrayList<Event>> toUserTuples = new PairFunction<String, Tuple2<String, String>, ArrayList<Event>>() {
            @Override
            public Tuple2<Tuple2<String,String>, ArrayList<Event>> call(String s) throws Exception
            {
                String[] eventArray = s.split("\t");

                /* Make the key */
                Tuple2<String, String> userTupleKey = new Tuple2<String, String>
                        (eventArray[Globals.userIdIndex], eventArray[Globals.referringDomainIndex]);

                /* Make the value */
                ArrayList<Event> userTupleValue = new ArrayList<Event>();
                userTupleValue.add( new Event(eventArray[Globals.eventTypeIndex], eventArray[Globals.timeStampIndex]) );

                return new Tuple2<Tuple2<String, String>, ArrayList<Event>>(userTupleKey, userTupleValue);
            }
        };

        /* Transformation2 - accumulate the events */
        Function2<ArrayList<Event>, ArrayList<Event>, ArrayList<Event>> sumFunction = new Function2<ArrayList<Event>, ArrayList<Event>, ArrayList<Event>>() {
            @Override
            public ArrayList<Event> call(ArrayList<Event> events, ArrayList<Event> events2) throws Exception {
                ArrayList<Event> resultList = new ArrayList<Event>();
                resultList.addAll(events);
                resultList.addAll(events2);
                return resultList;
            }
        };

        /* Events grouped into sessions */
        JavaPairRDD<Tuple2<String, String>, ArrayList<Event>> baseSession = input.mapToPair(toUserTuples).reduceByKey(sumFunction);

        /* Filter out events with more than 1K events and also sort the events by time stamp */
        PairFlatMapFunction<Tuple2<Tuple2<String, String>, ArrayList<Event>>, Tuple2<String, String>, TreeSet<Event>> filterEvents =
                new PairFlatMapFunction<Tuple2<Tuple2<String, String>, ArrayList<Event>>,Tuple2<String, String>, TreeSet<Event>>() {
            @Override
            public Iterable<Tuple2<Tuple2<String, String>, TreeSet<Event>>> call(Tuple2<Tuple2<String, String>, ArrayList<Event>> session) throws Exception {
                ArrayList<Tuple2<Tuple2<String, String>, TreeSet<Event>>> returnSession = new ArrayList<Tuple2<Tuple2<String, String>, TreeSet<Event>>>();

                TreeSet<Event> eventTree = new TreeSet<Event>();
                if ( ! (session._2().size() > 1000) )
                {
                    eventTree.addAll(session._2());
                    returnSession.add(new Tuple2<Tuple2<String, String>, TreeSet<Event>>(session._1(), eventTree));
                }

                return returnSession;
            }
        };

        /* Filter(Events) and sort(Events) the sessions */
        JavaPairRDD<Tuple2<String, String>, TreeSet<Event>> sessionsFiltered = baseSession.flatMapToPair(filterEvents);

        /* Sort the sessions by user_id and partition based off referring domain */
        JavaPairRDD<Tuple2<String, String>, TreeSet<Event>> partitionedAndUserSorted = sessionsFiltered.sortByKey(new UserKeyComparator()).partitionBy(new CustomPartitioner());

        partitionedAndUserSorted.saveAsTextFile(outputFilename);


    }
}
